﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace CS481_Calculator.Interpreter
{
    class Number : Expression
    {
        private double value;

        private Number(Expression _father, Expression _term1, Expression _term2) : base(_father, _term1, _term2) { }

        public Number(Expression _father, double value) : base(null, null, null)
        {
            this.father = _father;
            this.value = value;
        }

        public override double Calculate()
        {
            return value;
        }

        public override void printType()
        {
            Debug.WriteLine("Number: " + value);
        }
        public override Expression getRightNull()
        {
            return this.father.getRoot();
        }

        public override Expression findFirstPriority()
        {
            return null;
        }

        public override bool isComplete()
        {
            return true; 
        }

    }
}
