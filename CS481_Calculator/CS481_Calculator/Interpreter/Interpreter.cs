﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace CS481_Calculator.Interpreter
{
    class Interpreter
    {

        public static double ProcessExpression(string expression)
        {
            //expression += "#";
            if (!Char.IsDigit(expression[expression.Length - 1])) return 0;
            Expression root = null;
            string number = "";
            if(expression[0] == '-')
            {
                number += "-";
                expression = expression.Substring(1);
            }
            foreach(char c in expression)
            {
                if (Char.IsDigit(c) || c == '.')
                {
                    number += c;
                }
                else
                {
                    if(root == null)
                    {
                        if(c == '*')
                        {
                            root = new Multiplication(null, new Number(root, Double.Parse(number)), null);
                        }else if(c == '/')
                        {
                            root = new Division(null, new Number(root, Double.Parse(number)), null);
                        }
                        else if(c == '+')
                        {
                            root = new Sum(null, new Number(root, Double.Parse(number)), null);
                        }
                        else if(c == '-')
                        {
                            root = new Subtraction(null, new Number(root, Double.Parse(number)), null);
                        }
                        Debug.WriteLine(number);
                        number = "";
                    }
                    else
                    {
                        Expression next = root.getRightNull();
                        if(next is Sum || next is Subtraction)
                        {
                            if (c == '*')
                            {
                                next.term2 = new Multiplication(next, new Number(next.term2, Double.Parse(number)), null);
                            }
                            else if (c == '/')
                            {
                                next.term2 = new Division(next, new Number(next.term2, Double.Parse(number)), null);
                            }
                        }
                        else
                        {
                            if (c == '*')
                            {
                                next.term2 = new Number(next, Double.Parse(number));
                                if (next == root)
                                {
                                    root = new Multiplication(null, root, null);
                                    root.term1.father = root;
                                }
                                else
                                {
                                    next.father.term2 = new Multiplication(next.father, next, null);
                                }
                            }
                            else if (c == '/')
                            {
                                next.term2 = new Number(next, Double.Parse(number));
                                if (next == root)
                                {
                                    root = new Division(null, root, null);
                                    root.term1.father = root;
                                }
                                else
                                {
                                    next.father.term2 = new Division(next.father, next, null);
                                }
                            }
                        }
                        if (c == '+')
                        {
                            next.term2 = new Number(next, Double.Parse(number));
                            if (root.isComplete())
                            {
                                root = new Sum(null, root, null);
                                root.term1.father = root;
                            }
                            else
                            {
                                next = new Sum(next.father, next, null);
                            }
                        }
                        else if (c == '-')
                        {
                            next.term2 = new Number(next, Double.Parse(number));
                            if (root.isComplete())
                            {
                                root = new Subtraction(null, root, null);
                                root.term1.father = root;
                            }
                            else
                            {
                                next = new Subtraction(next.father, next, null);
                            }
                        }
                        Debug.WriteLine(number);
                        number = "";
                    }
                }
            }
            if (number == "" || root == null) return 0;
            Debug.WriteLine(number);
            root.getRightNull().term2 = new Number(root, Double.Parse(number));
            root.printType();
            return root.Calculate();
        }
    }
}
