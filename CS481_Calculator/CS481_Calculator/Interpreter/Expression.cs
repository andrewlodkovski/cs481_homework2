﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace CS481_Calculator.Interpreter
{
    class Expression
    {
        public Expression term1;
        public Expression term2;
        public Expression father;
        public Expression(Expression _father, Expression _term1, Expression _term2)
        {
            father = _father;
            term1 = _term1;
            term2 = _term2;
        }

        public virtual double Calculate()
        {
            return 0;
        }

        public virtual Expression getRightNull()
        {
            if (term2 == null) return this;

            return term2.getRightNull();

        }

        public virtual bool isComplete()
        {
            return term1 != null && term2 != null && term1.isComplete() && term2.isComplete();
        }

        public virtual Expression findFirstPriority()
        {
            if (term2 == null) return null;
            return term2.findFirstPriority();
        }
        
        public Expression getRoot()
        {
            if (this.father == null) return this;
            return this.father.getRoot();
        }

        public virtual void printType()
        {
            Debug.WriteLine(this.GetType());
            if(term1!=null) term1.printType();
            if(term2!=null) term2.printType();
        }

    }
}
