﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace CS481_Calculator.Interpreter
{
    class Multiplication : Expression
    {
        public Multiplication(Expression _father, Expression _term1, Expression _term2) : base(_father, _term1, _term2) { }

        public override double Calculate()
        {
            double t1 = term1.Calculate();
            double t2 = term2.Calculate();
            Debug.WriteLine(t1 + "*" + t2);
            return term1.Calculate() * term2.Calculate();
        }

        public override Expression findFirstPriority()
        {
            return this;
        }
    }
}
