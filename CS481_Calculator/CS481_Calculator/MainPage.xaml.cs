﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using CS481_Calculator.Interpreter;

namespace CS481_Calculator
{
    public partial class MainPage : ContentPage
    {
        private string expression = "0";
        public string Expression {
            get {
                return expression;
            } set {
                expression = value;
                OnPropertyChanged("Expression");
            }
        }
        private double fontSize = 30;
        public double FontSize
        {
            get
            {
                return fontSize;
            }
            set
            {
                fontSize = value;
                OnPropertyChanged("FontSize");
            }
        }
        public MainPage()
        {
            BindingContext = this;
            InitializeComponent();
        }

        void UpdateFontSize()
        {
            FontSize = Math.Max(15,30 - (Expression.Length/3));
        }

        void UpdateExpression(string expr)
        {
            if (Expression.Length >= 105) return;
            if (!Char.IsDigit(expr[0]) && !Char.IsDigit(Expression[Expression.Length - 1]) && expr != ".") return;
            if (Expression == "0" && Char.IsDigit(expr[0])) Expression = "";

            Expression += expr;
            UpdateFontSize();
        }

        void OnTap0(object sender, EventArgs args)
        {
            if (Expression.Length > 0 && Expression[Expression.Length - 1] == '/') return;
            UpdateExpression("0");
        }

        void OnTap1(object sender, EventArgs args)
        {
            UpdateExpression("1");
        }
        
        void OnTap2(object sender, EventArgs args)
        {
            UpdateExpression("2");
        }

        void OnTap3(object sender, EventArgs args)
        {
            UpdateExpression("3");
        }

        void OnTap4(object sender, EventArgs args)
        {
            UpdateExpression("4");
        }

        void OnTap5(object sender, EventArgs args)
        {
            UpdateExpression("5");
        }

        void OnTap6(object sender, EventArgs args)
        {
            UpdateExpression("6");
        }

        void OnTap7(object sender, EventArgs args)
        {
            UpdateExpression("7");
        }

        void OnTap8(object sender, EventArgs args)
        {
            UpdateExpression("8");
        }

        void OnTap9(object sender, EventArgs args)
        {
            UpdateExpression("9");
        }

        void OnTapC(object sender, EventArgs args)
        {
            Expression = "0";
        }

        void OnTapMulti(object sender, EventArgs args)
        {
            UpdateExpression("*");
        }

        void OnTapDiv(object sender, EventArgs args)
        {
            UpdateExpression("/");
        }

        void OnTapPlus(object sender, EventArgs args)
        {
            UpdateExpression("+");
        }

        void OnTapMinus(object sender, EventArgs args)
        {
            UpdateExpression("-");
        }

        void OnTapDot(object sender, EventArgs args)
        {
            if (Expression.Length == 0) return;
            for(int i = Expression.Length-1; i >= 0; i--)
            {
                if (!Char.IsDigit(Expression[i]) && Expression[i] != '.') break;
                else if (Expression[i] == '.') return;
            }
            UpdateExpression(".");
        }

        void OnTapEquals(object sender, EventArgs args)
        {
            Expression = CS481_Calculator.Interpreter.Interpreter.ProcessExpression(Expression).ToString();
        }
    }
}
